//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include <VPL/Base/Singleton.h>

//============== Definitions for test class objects ============================
namespace singleton
{
std::string requiredOutput;

//! Singleton CA.
//! - Short longevity.
//! - Public constructor allows user instantiation!
class Ca : public vpl::base::CSingleton<vpl::base::SL_SHORT>
{
public:
	//! Default constructor
	Ca() { requiredOutput += "CA::CA()#"; }

	//! Destructor
	~Ca() { requiredOutput += "CA::~CA()#"; }

	//! Print message
	void print() { requiredOutput += "CA::print()#"; }

};


//! Singleton CAA - short longevity.
//! - User cannot make instances :)
class Caa : public vpl::base::CSingleton<vpl::base::SL_SHORT>
{
public:
	//! Destructor
	~Caa() { requiredOutput += "CAA::~CAA()#"; }

	//! Print a message
	void print() { requiredOutput += "CAA::print()#"; }

private:
	//! Private constructor
	Caa() { requiredOutput += "CAA::CAA()#"; }

	VPL_PRIVATE_SINGLETON(Caa);
};

//! Singleton CB.
//! - Middle longevity.
//! - Public constructor allows user instantiation!
class Cb : public vpl::base::CSingleton<vpl::base::SL_MIDDLE>
{
public:
	//! Default constructor.
	Cb() { requiredOutput += "CB::CB()#"; }

	//! Destructor.
	~Cb() { requiredOutput += "CB::~CB()#"; }

	//! Print message.
	void print() { requiredOutput += "CB::print()#"; }

};

//! Singleton CC.
//! - Long longevity.
//! - Public constructor allows user instantiation!
class Cc : public vpl::base::CSingleton<vpl::base::SL_LONG>
{
public:
	//! Default constructor.
	Cc() { requiredOutput += "CC::CC()#"; }

	//! Destructor.
	~Cc() { requiredOutput += "CC::~CC()#"; }

	//! Print message.
	void print() { requiredOutput += "CC::print()#"; }

};

//! Singleton CCC.
//! - Long longevity.
//! - User cannot make instances :)
class Ccc : public vpl::base::CSingleton<vpl::base::SL_LONG>
{
public:
	//! Destructor.
	~Ccc() { requiredOutput += "CCC::~CCC()#"; }

	//! Print message.
	void print() { requiredOutput += "CCC::print()#"; }

private:
	//! Private constructor.
	Ccc() { requiredOutput += "CCC::CCC()#"; }

	VPL_PRIVATE_SINGLETON(Ccc);
};


//! Singleton CD.
//! - Default longevity.
//! - Public constructor allows user instantiation!
class Cd : public vpl::base::CDefaultSingleton
{
public:
	//! Public constructor.
	Cd() { requiredOutput += "CD::CD()#"; }

	//! Destructor.
	~Cd() { requiredOutput += "CD::~CD()#"; }

	//! Print message.
	void print() { requiredOutput += "CD::print()#"; }

};


//! Singleton CS.
//! - Library longevity.
//! - Public constructor allows user instantiation!
class Cs : public vpl::base::CLibrarySingleton
{
public:
	//! Public constructor.
	Cs() { requiredOutput += "CS::CS()#"; }

	//! Destructor.
	~Cs() { requiredOutput += "CS::~CS()#"; }

	//! Print message.
	void print() { requiredOutput += "CS::print()#"; }

};

//============== Test code =========================
//! Gets the singleton instances.
void getInstances()
{
	// Get the singleton instances
	Cs& s = VPL_SINGLETON(Cs);
	Ca& a = VPL_SINGLETON(Ca);
	Cc& c = VPL_SINGLETON(Cc);
	Cb& b = VPL_SINGLETON(Cb);
	Cd& d = VPL_SINGLETON(Cd);
	Caa& aa = VPL_SINGLETON(Caa);
	Ccc& cc = VPL_SINGLETON(Ccc);

	// Call the print() method
	s.print();
	a.print();
	c.print();
	b.print();
	d.print();
	aa.print();
	cc.print();
}

//! Get Singleton Instances
TEST(SingletonTest, Instances)
{
	//! Get Singleton Instances 1
	getInstances();

	std::string reqString1 = "CS::CS()#CA::CA()#CC::CC()#CB::CB()#CD::CD()#CAA::CAA()#";
	reqString1 += "CCC::CCC()#CS::print()#CA::print()#CC::print()#CB::print()#CD::print()#CAA::print()#CCC::print()#";
	ASSERT_STREQ(reqString1.c_str(), requiredOutput.c_str());
	requiredOutput.clear();

	//! Get Singleton Instances 2
	getInstances();

	std::string reqString2 = "CS::print()#CA::print()#CC::print()#CB::print()#CD::print()#CAA::print()#CCC::print()#";
	ASSERT_STREQ(reqString2.c_str(), requiredOutput.c_str());
	requiredOutput.clear();
}
} // namespace factory