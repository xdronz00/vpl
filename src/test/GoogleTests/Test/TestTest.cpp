//==============================================================================
/* This file is part of
*
* VPL - Voxel Processing Library
* Copyright 2014 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#include "gtest/gtest.h"
#include <stdio.h>
#include <VPL/Test/Utillities/arguments.h>


GTEST_API_ int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);

	vpl::test::Arguments& testArgv = vpl::test::Arguments::get();
	testArgv.allow("dir");
	testArgv.init(argc, argv);

    return RUN_ALL_TESTS();
}

