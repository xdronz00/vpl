//==============================================================================
/* This file is part of
 *
 * VPL - Voxel Processing Library
 * Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 *
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 */

#ifndef VPL_Filename_H
#define VPL_Filename_H

#include <VPL/Base/Setup.h>


namespace vpl
{

//==============================================================================
/*!
 *
 */


} // namespace vpl

#endif // VPL_Filename_H
