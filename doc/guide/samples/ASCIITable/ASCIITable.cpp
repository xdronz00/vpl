//==============================================================================
/*
 * This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 *
 * Medical Data Segmentation Toolkit (MDSTk)    \n
 * Copyright (c) 2003-2010 by Michal Spanel     \n
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2006/10/30                          \n
 *
 * File description:
 * - Sample application.
 */

#include <MDSTk/Image/mdsImage.h>
#include <MDSTk/Image/mdsImageFunctions.h>

// STL
#include <iostream>


int main(int argc, char *argv[])
{
    // Image/table of characters
    typedef mds::img::CImage<unsigned char> tTable;

    // Fill the table
    tTable Table(25, 10);
    Table.fill(' ');
    tTable::tIterator it(Table);
    for( unsigned char ucValue = 32; it; ++it, ++ucValue )
    {
        if( (*it = ucValue) == 255 )
        {
            break;
        }
    }

    // Print the table
    std::cout << "Table of ASCII characters starting with space:" << std::endl;
    for( mds::tSize j = 0; j < Table.getYSize(); ++j )
    {
        for( mds::tSize i = 0; i < Table.getXSize(); ++i )
        {
            std::cout << Table(i, j) << ' ';
        }
        std::cout << std::endl;
    }

    return 0;
}

