//==============================================================================
/*
 * This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 *
 * Medical Data Segmentation Toolkit (MDSTk)    \n
 * Copyright (c) 2003-2010 by Michal Spanel     \n
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2006/10/30                          \n
 *
 * File description:
 * - Sample module.
 */

#include "Slice2Text.h"

// MDSTk
#include <MDSTk/Math/mdsBase.h>
#include <MDSTk/Image/mdsPixelTraits.h>
#include <MDSTk/Image/mdsSlice.h>
#include <MDSTk/Image/mdsImageFunctions.h>

// STL
#include <sstream>


//==============================================================================
/*
 * Global constants.
 */

//! Module description.
const std::string MODULE_DESCRIPTION    = "Sample module providing conversion of an input image to text.";

//! Characters used to interpolate gray levels.
//const std::string GRAY2CHAR             = "01 ";
//const std::string GRAY2CHAR             = "# ";
//const std::string GRAY2CHAR             = "@%#*+=-:. ";
const std::string GRAY2CHAR             = "#WMNRXVYIti+=;:,. ";

//! Size of the character in pixels.
const mds::tSize CHAR_XSIZE             = 7;
const mds::tSize CHAR_YSIZE             = 12;


//==============================================================================
/*
 * Implementation of the class CSlice2Text.
 */
CSlice2Text::CSlice2Text(const std::string& sDescription)
    : mds::mod::CModule(sDescription)
{
}


CSlice2Text::~CSlice2Text()
{
}


bool CSlice2Text::startup()
{
    // Note
    MDS_LOG_NOTE("Module startup");

    // Test existence of input and output channel
    if( getNumOfInputs() != 1 || getNumOfOutputs() != 1 )
    {
        MDS_CERR('<' << m_sFilename << "> Wrong number of input and output channels" << std::endl);
        return false;
    }

    // O.K.
    return true;
}


bool CSlice2Text::main()
{
    // Note
    MDS_LOG_NOTE("Module main function");

    // I/O channels
    mds::mod::CChannel *pIChannel = getInput(0);
    mds::mod::CChannel *pOChannel = getOutput(0);

    // Is any input?
    if( !pIChannel->isConnected() )
    {
        return false;
    }

    // Create a new slice
    mds::img::CSlicePtr spSlice(new mds::img::CSlice());

    // Wait for data
    if( pIChannel->wait(1000) )
    {
        // Read slice from the input channel
        if( readInput(pIChannel, spSlice.get()) )
        {
            //! Allowed pixel values.
            double dMin = mds::img::getMin<double>(*spSlice);
            double dMax = mds::img::getMax<double>(*spSlice);

            //! Maximal character index
            int iMax = int(GRAY2CHAR.size()) - 1;

            // Estimate conversion of gray levels to text
            double dConv = iMax / (dMax - dMin);

            // Estimate size of the final ASCII image
            mds::tSize XSize = spSlice->getXSize() / CHAR_XSIZE;
            mds::tSize YSize = spSlice->getYSize() / CHAR_YSIZE;

            // Prepare output ASCII image
            mds::img::CImage8 Image8(XSize, YSize, 0);
            Image8.fill(' ');

            // Fill the ASCII image
            for( mds::tSize j = 0; j < Image8.getYSize(); ++j )
            {
                for( mds::tSize i = 0; i < Image8.getXSize(); ++i )
                {
                    // Create subimage of the original image
                    mds::img::CDImage SubImage(*spSlice,
                                               i * CHAR_XSIZE, j * CHAR_YSIZE,
                                               CHAR_XSIZE, CHAR_YSIZE,
                                               mds::REFERENCE
                                               );

                    // Compute index of the character
                    int iIndex = mds::math::round2Int((mds::img::getMean<double>(SubImage) - dMin) * dConv);

                    // Set the character
                    Image8(i, j) = GRAY2CHAR[iIndex];
                }
            }

            // Write result to the string
            std::stringstream ss;

            // Write 8-bit image to the string
            for( mds::tSize j = 0; j < Image8.getYSize(); ++j )
            {
                for( mds::tSize i = 0; i < Image8.getXSize(); ++i )
                {
                    ss << Image8(i, j);
                }
                ss << std::endl;
            }

            // Write result to the output channel
            if( !pOChannel->write(ss.str().c_str(), mds::tSize(ss.str().size())) )
            {
                MDS_CERR('<' << m_sFilename << "> Failed to write the output text" << std::endl);
                return false;
            }
        }
        else
        {
            MDS_CERR('<' << m_sFilename << "> Failed to read input slice" << std::endl);
            return false;
        }
    }
    else
    {
        MDS_LOG_NOTE("Wait timeout");
    }

    // Returning true means to continue processing the input channel
    return true;
}


void CSlice2Text::shutdown()
{
    // Note
    MDS_LOG_NOTE("Module shutdown");
}


void CSlice2Text::writeExtendedUsage(std::ostream& Stream)
{
}


//==============================================================================
/*
 * Function main() which creates and executes the console application.
 */
int main(int argc, char *argv[])
{
    // Creation of the module using smart pointer
    CSlice2TextPtr spModule(new CSlice2Text(MODULE_DESCRIPTION));

    // Initialize and execute the module
    if( spModule->init(argc, argv) )
    {
        spModule->run();
    }

    // Console application finished
    return 0;
}

